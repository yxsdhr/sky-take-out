package com.sky.dto;


import lombok.Data;

import java.io.Serializable;

/**
 * 用于接收登录的数据的DTO
 */
@Data
public class EmployeeLoginDTO implements Serializable {

    private String username;

    private String password;

}
