package com.sky.web.admin;

import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.EmployeeService;
import com.sky.vo.EmployeeLoginVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 员工管理
 */
@Slf4j
@RestController
@RequestMapping("/admin/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping("login")
    public Result login(@RequestBody EmployeeLoginDTO employeeLoginDTO) {
        EmployeeLoginVO vo = employeeService.login(employeeLoginDTO);
        return Result.success(vo);
    }

    @PostMapping("logout")
    public Result logout() {
//        前端进行了操作，只需要返回Result,前端就会自动清空浏览器中的Cookie
        return Result.success();
    }

    @GetMapping("page")
    public Result findByPage(EmployeePageQueryDTO dto) {
        PageResult pageResult = employeeService.findBypage(dto);
        return Result.success(pageResult);
    }

    @PostMapping
    public Result save(@RequestBody EmployeeDTO employeeDTO) {
        employeeService.save(employeeDTO);
        return Result.success();
    }

    @GetMapping("{id}")
    public Result findById(@PathVariable Long id) {
        Employee employee = employeeService.findById(id);
        return Result.success(employee);
    }

    @PutMapping
    public Result update(@RequestBody EmployeeDTO employeeDTO) {
        employeeService.update(employeeDTO);
        return Result.success();
    }

    @PostMapping("status/{status}")
    public Result updateStatus(@PathVariable Integer status,
                               @RequestParam Long id) {
        employeeService.updateStatus(id, status);
        return Result.success();
    }
}
