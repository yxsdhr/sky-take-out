package com.sky.interceptor;

import com.sky.constant.JwtClaimsConstant;
import com.sky.context.ThreadLocalUtil;
import com.sky.properties.JwtProperties;
import com.sky.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import org.apache.el.parser.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AdminLoginInterceptor implements HandlerInterceptor {
    @Autowired
    private JwtProperties jwtProperties;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        try {
            Claims claims = JwtUtil.parseJWT(jwtProperties.getAdminSecret(), token);//传入盐和token ,进行解析，解析失败返回401，解析成功后返回荷载中的内容
            Long empId = Long.valueOf(claims.get(JwtClaimsConstant.EMP_ID).toString());//解析成功后。从荷载中取出的id值
            ThreadLocalUtil.setCurrentId(empId);//将id绑定到本线程中，只要本线程还在运行，线程内的任何位置都可以取出id的值
            return true;//token解析成功后需要放行

        } catch (Exception e) {
            response.setStatus(401);//token解析失败
            return false;//拦截器直接拦截
        }
        }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//    remove原因是线程解除信息绑定，因为线程用完之后要还给线程池，要把线程处理的干干净净的再还回去，如果不清楚，下一次再用到线程的话，线程还会存在这个值
        ThreadLocalUtil.removeCurrentId();
    }
}
