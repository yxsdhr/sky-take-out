package com.sky.mapper;

import com.sky.entity.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    @Select("select * from employee where username = #{username}")
    Employee findByUserName(String username);

    List<Employee> findList(String name);

    void save(Employee employee);

    @Select("select * from employee where id=#{id}")
    Employee findById(Long id);

    void updateById(Employee employee);
}
