package com.sky.service;

import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.result.PageResult;
import com.sky.vo.EmployeeLoginVO;

public interface EmployeeService {


    EmployeeLoginVO login(EmployeeLoginDTO employeeLoginDTO);

    PageResult findBypage(EmployeePageQueryDTO dto);

    void save(EmployeeDTO employeeDTO);

    Employee findById(Long id);

    void update(EmployeeDTO employeeDTO);

    void updateStatus(Long id, Integer status);
}
