package com.sky.service.impl;

import ch.qos.logback.classic.pattern.ClassOfCallerConverter;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.JwtClaimsConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.ThreadLocalUtil;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.exception.BusinessException;
import com.sky.mapper.EmployeeMapper;
import com.sky.properties.JwtProperties;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import com.sky.utils.JwtUtil;
import com.sky.vo.EmployeeLoginVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private JwtProperties jwtProperties;

    @Override
    public EmployeeLoginVO login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();
// .if自动加if
        if (StrUtil.isBlank(username) || StrUtil.isBlank(password)) {
            throw new BusinessException("账号、密码不能为空");
        }
        Employee employee = employeeMapper.findByUserName(username);

        if (employee == null) {
            throw new BusinessException("用户名错误");
        }
        if (!SecureUtil.md5(password).equals(employee.getPassword())) {
            throw new BusinessException("密码错误");
        }
//        包装类比较最好用equals
        if (StatusConstant.DISABLE.equals(employee.getStatus())) {
            throw new BusinessException("员工已禁用");
        }
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.EMP_ID, employee.getId());
        String token = JwtUtil.createJWT(
                jwtProperties.getAdminSecret(),//盐
                jwtProperties.getAdminTtl(),//token存留时间
                claims);//有效荷载了在生成的，保存了用户的id
        return EmployeeLoginVO.builder()
                .id(employee.getId())
                .name(employee.getName())
                .userName(employee.getUsername())
                .token(token)
                .build();
    }

    @Override
    public PageResult findBypage(EmployeePageQueryDTO dto) {
//        从dto获取页数以及每页的数据条数
        int page = dto.getPage();
        int pageSize = dto.getPageSize();

//        对获取的数据进行校验，如果值是0的话，进行赋值
        if (page == 0) {
            page = 1;
        }
        if (pageSize == 0) {
            pageSize = 10;
        }

//       把分页参数添加到PageHelper
        PageHelper.startPage(page, pageSize);
//       按照条件进行查询
        List<Employee> list = employeeMapper.findList(dto.getName());
//        将List强转为page类，可以调用getTotal总数
        Page p = (Page) list;
        return new PageResult(p.getTotal(), list);
    }

    @Override
    public void save(EmployeeDTO employeeDTO) {
//        获取前端填写的数据
        String name = employeeDTO.getName();
        String username = employeeDTO.getUsername();
        String sex = employeeDTO.getSex();
        String phone = employeeDTO.getPhone();
        String idNumber = employeeDTO.getIdNumber();
//        对填写的数据进行非空验证
        if (StrUtil.isBlank(username) || StrUtil.isBlank(name) || StrUtil.isBlank(sex)
                || StrUtil.isBlank(phone) || StrUtil.isBlank(idNumber)) {
            throw new BusinessException("账号不能为空");
        }

//        对填写的数据格式进行验证，正则表达式进行匹配
        if (!username.matches("[a-zA-Z0-9]{3,24}")) {
            throw new BusinessException("账号输入不符，请输入3-20个符号");
        }
        if (!name.matches("[a-zA-Z\\u4e00-\\u9fa5]{1,12}")) {
            throw new BusinessException("姓名输入不符，请输入1-12个符号或汉字");
        }
        if (!phone.matches("1[3456789]\\d{9}")) {
            throw new BusinessException("请输入正确的手机号");
        }
        if (StrUtil.isBlank(idNumber)) {
            throw new BusinessException("身份证号号码不正确");
        }

//        通过用户名进行查找，如果查找到，书名用户名存在，直接抛异常
        Employee employee = employeeMapper.findByUserName(username);
        if (employee != null) {
            throw new BusinessException("账号重复，请调整");
        }

//        每抛异常，说明employee指向的是null
        employee = new Employee();

//      建议使用工具类进行同名属性拷贝操作，hutools,参数1带有数据的对象，参数2目标对象
        BeanUtil.copyProperties(employeeDTO, employee);

//        补充表中必要的数据
        employee.setPassword(PasswordConstant.DEFAULT_PASSWORD);
        employee.setStatus(StatusConstant.ENABLE);
        employee.setCreateTime(LocalDateTime.now());
        employee.setUpdateTime(LocalDateTime.now());
        employee.setCreateUser(ThreadLocalUtil.getCurrentId());
        employee.setUpdateUser(ThreadLocalUtil.getCurrentId());

//        调用mapper进行保存
        employeeMapper.save(employee);

    }

    @Override
    public Employee findById(Long id) {
//        用于回显，通过id进行查找
        return employeeMapper.findById(id);
    }

    @Override
    public void update(EmployeeDTO employeeDTO) {
        //        获取前端填写的数据
        String name = employeeDTO.getName();
        String username = employeeDTO.getUsername();
        String sex = employeeDTO.getSex();
        String phone = employeeDTO.getPhone();
        String idNumber = employeeDTO.getIdNumber();
//        对填写的数据进行非空验证
        if (StrUtil.isBlank(username) || StrUtil.isBlank(name) || StrUtil.isBlank(sex)
                || StrUtil.isBlank(phone) || StrUtil.isBlank(idNumber)) {
            throw new BusinessException("账号不能为空");
        }

//        对填写的数据格式进行验证，通过正则表达式进行匹配1
        if (!username.matches("[a-zA-Z0-9]{3,24}")) {
            throw new BusinessException("账号输入不符，请输入3-20个符号");
        }
        if (!name.matches("[a-zA-Z\\u4e00-\\u9fa5]{1,12}")) {
            throw new BusinessException("姓名输入不符，请输入1-12个符号或汉字");
        }
        if (!phone.matches("1[3456789]\\d{9}")) {
            throw new BusinessException("请输入正确的手机号");
        }
        if (StrUtil.isBlank(idNumber)) {
            throw new BusinessException("身份证号号码不正确");
        }

//       id不能为空，需要根据id进行更新
        if (employeeDTO.getId()==null) {
            throw new BusinessException("id不能为空");
        }

//        id不能重复，如果id更改，那么从数据查到的employee应该是null,
//        如果未修改，那么获取的employee的id不能等于修改的id
        Employee employee = employeeMapper.findByUserName(employeeDTO.getUsername());
        if (employee != null && !employeeDTO.getId().equals(employee.getId())) {
            throw new BusinessException("账号重复请调整");
        }
//      没抛异常，说明employee指向的是null
        employee = new Employee();
        BeanUtil.copyProperties(employeeDTO, employee);
//      补充数据
        employee.setUpdateTime(LocalDateTime.now());
        employee.setUpdateUser(ThreadLocalUtil.getCurrentId());
//      调用Mapper进行更新
        employeeMapper.updateById(employee);

    }

    @Override
    public void updateStatus(Long id, Integer status) {
//
        Employee employee = Employee.builder()
                .id(id)
                .status(status)
                .updateTime(LocalDateTime.now())
                .updateUser(ThreadLocalUtil.getCurrentId())
                .build();
        employeeMapper.updateById(employee);

    }
}
